class CameroCar < Car

    def initialize(max_speed = 200, brand = 'Chevy')
        super(max_speed, brand)
    end

end
