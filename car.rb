class Car
    attr_accessor :current_speed, :max_speed, :brand

    def initialize max_speed=200, brand='unkown', current_speed=0
        @current_speed = current_speed
        @brand = brand
        @max_speed = max_speed
    end

    def accelerate
        @current_speed += 1
        puts "Current of the Car is  ---> #{@current_speed}"
        drive
    end

    def drive
        @current_speed < @max_speed ? accelerate : "Car hit the Maximum Speed #{@max_speed}"
    end
end
